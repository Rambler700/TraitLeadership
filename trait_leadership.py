# -*- coding: utf-8 -*-
"""
Created on Fri Feb 23 01:15:45 2018

@author: enovi
"""

import nltk

#Trait leadership analyzer

def main():
    choice = True
    article_text = ''
    c = 'y'
    print('Welcome to Trait Leadership Analyzer!')
    while choice == True:
         c = input('Analyze an article y/n? ')
         if c == 'y':
             article_text = get_text()
             analyze_text(article_text)
             choice = True
         elif c == 'n':
             choice = False
             
def get_text():
    article_text = ''
    article_text = str(input('Enter the message here: '))
    return article_text

def analyze_text(article_text):
    #analyze 7 leadership traits
    #words that indicate high levels of the trait
    #abbreviated form of trait_high_list
    control_hl     = ['I', 'me', 'mine', 'my', 'self']
    
    pow_need_hl    = ['affirm','declare','assert','challenge','break','crush','smash',
                      'compel','condemn','oppose','demonstrate','desire','disclose',
                      'remove','eradicate','dispel']
    
    conc_comp_hl   = ['affirm', 'assert', 'break', 'challenge', 'compel', 'condemn', 'crush',
                      'declare', 'demonstrate', 'desire', 'disclose', 'dispel', 'eradicate',
                      'oppose', 'remove', 'smash']
    
    self_conf_hl   = ['act', 'confident', 'crucial', 'decide', 'definitive', 'determine',
                      'dictate', 'direct', 'final', 'govern', 'key', 'pivot', 'will']
    
    ps_focus_hl    = ['accomplishment', 'achieve', 'focus', 'goal', 'plan', 'proposal',
                      'recommendation', 'schedule', 'strategy', 'tactic']
    
    distrust_hl    = ['agreement', 'armistice', 'concord', 'contract', 'convention', 'covenant',
                      'deal', 'entente', 'pact', 'peace', 'protocol', 'treaty']
    
    ig_bias_hl     = ['best', 'capable', 'civilize', 'defend', 'great', 'our', 'power',
                      'progress', 'prosper', 'strength', 'strong', 'success', 'superior', 'them',
                      'they', 'us', 'we']
    
    #words that indicate low levels of the trait, or the opposite trait
    #abbreviated form of trait_low_list
    control_ll     = ['care', 'caution', 'circumspect', 'discretion', 'heed', 'prudence',
                      'risk', 'skeptic', 'suggest']
    
    pow_need_ll    = ['accomodate', 'adapt', 'alter', 'bargain', 'compromise', 'contemplate',
                      'contradict', 'contribute', 'enhance', 'enlighten', 'negotiate',
                      'satisfy', 'validate']
    
    conc_comp_ll   = ['absolute', 'certain', 'consensus', 'correct', 'fact', 'majority',
                      'right', 'true', 'truth', 'valid']
    
    self_conf_ll   = ['confuse', 'likely', 'might', 'need', 'perplex', 'probably', 'suspects',
                      'typical', 'usual', 'worried']
    
    ps_focus_ll    = ['admiration', 'amnesty', 'appreciation', 'emancipate', 'esteem',
                      'forgive', 'free', 'harm', 'liberate', 'regard', 'rescue', 'respect',
                      'suffer']
    
    distrust_ll    = ['conflict', 'danger', 'devious', 'doubt', 'enemy', 'extreme', 'foe',
                      'ideology', 'liar', 'propaganda', 'risk', 'sanction', 'shady', 'threat',
                      'wary', 'wrong']
    
    ig_bias_ll     = ['aid', 'assist', 'cosmopolitan', 'culture', 'explore', 'global', 'group',
                      'intercontinental', 'international', 'open', 'region', 'universal',
                      'urban', 'world']

    trait_total = 0
    
    #high trait count
    control_h   = 0
    pow_need_h  = 0
    conc_comp_h = 0
    self_conf_h = 0
    ps_focus_h  = 0
    distrust_h  = 0
    ig_bias_h   = 0 
    
    #low trait count
    control_l   = 0
    pow_need_l  = 0
    conc_comp_l = 0
    self_conf_l = 0
    ps_focus_l  = 0
    distrust_l  = 0
    ig_bias_l   = 0 
    
    #total trait count
    control_c   = 0
    pow_need_c  = 0
    conc_comp_c = 0
    self_conf_c = 0
    ps_focus_c  = 0
    distrust_c  = 0
    ig_bias_c   = 0    
    
    article_text = article_text.lower()
    article_text = article_text.split(' ')
    
    #stemming
    for word in article_text:
        word = nltk.PorterStemmer().stem(word)
    
    for word in control_hl:
        word = nltk.PorterStemmer().stem(word)
    for word in pow_need_hl:
        word = nltk.PorterStemmer().stem(word)
    for word in conc_comp_hl:
        word = nltk.PorterStemmer().stem(word)
    for word in self_conf_hl:
        word = nltk.PorterStemmer().stem(word)
    for word in ps_focus_hl:
        word = nltk.PorterStemmer().stem(word)
    for word in distrust_hl:
        word = nltk.PorterStemmer().stem(word)
    for word in ig_bias_hl:
        word = nltk.PorterStemmer().stem(word)
    for word in control_ll:
        word = nltk.PorterStemmer().stem(word)
    for word in pow_need_ll:
        word = nltk.PorterStemmer().stem(word)
    for word in conc_comp_ll:
        word = nltk.PorterStemmer().stem(word)
    for word in self_conf_ll:
        word = nltk.PorterStemmer().stem(word)
    for word in ps_focus_ll:
        word = nltk.PorterStemmer().stem(word)
    for word in distrust_ll:
        word = nltk.PorterStemmer().stem(word)
    for word in ig_bias_ll:
        word = nltk.PorterStemmer().stem(word)
    
    #analysis
    for word in article_text:
        if word in control_hl:
            control_h   += 1
        elif word in pow_need_hl:
            pow_need_h  += 1
        elif word in conc_comp_hl:
            conc_comp_h += 1
        elif word in self_conf_hl:
            self_conf_h += 1
        elif word in ps_focus_hl:
            ps_focus_h  += 1
        elif word in distrust_hl:
            distrust_h  += 1
        elif word in ig_bias_hl:
            ig_bias_h   += 1
            
    for word in article_text:
        if word in control_ll:
            control_l   += 1
        elif word in pow_need_ll:
            pow_need_l  += 1
        elif word in conc_comp_ll:
            conc_comp_l += 1
        elif word in self_conf_ll:
            self_conf_l += 1
        elif word in ps_focus_ll:
            ps_focus_l  += 1
        elif word in distrust_ll:
            distrust_l  += 1
        elif word in ig_bias_ll:
            ig_bias_l   += 1        
        
    control_c   = control_h - control_l    
    pow_need_c  = pow_need_h - pow_need_l
    conc_comp_c = conc_comp_h - conc_comp_l
    self_conf_c = self_conf_h - self_conf_l
    ps_focus_c  = ps_focus_h - ps_focus_l
    distrust_c  = distrust_h - distrust_l
    ig_bias_c   = ig_bias_h - ig_bias_l
    
    trait_total = (control_c + pow_need_c + conc_comp_c + self_conf_c +
    ps_focus_c + distrust_c + ig_bias_c)
    
    #prevent divide by zero
    if trait_total == 0:
        trait_total = 1
    
    print('Belief that events can be controlled was {}.'.format(100 * control_c/trait_total))
    print('Need for power was {}.'.format(100 * pow_need_c/trait_total))
    print('Conceptual complexity was {}.'.format(100 * conc_comp_c/trait_total))
    print('Self confidence was {}.'.format(100 * self_conf_c/trait_total))
    print('Problem solving focus was {}.'.format(100 * ps_focus_c/trait_total))
    print('Distrust was {}.'.format(100 * distrust_c/trait_total))
    print('Ingroup bias was {}.'.format(100 * ig_bias_c/trait_total))
    
main()